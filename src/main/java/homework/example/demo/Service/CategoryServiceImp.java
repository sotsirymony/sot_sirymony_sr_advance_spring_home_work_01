package homework.example.demo.Service;

import homework.example.demo.Model.Articles;
import homework.example.demo.Model.Category;
import homework.example.demo.Repository.CategoryRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class CategoryServiceImp implements CategoryRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public Category post(Category category) {
        em.persist(category);
        return category;
    }

    @Override
    @Transactional
    public List<Category> findAll() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Category> query = criteriaBuilder.createQuery(Category.class);
        Root<Category> categoryRoot = query.from(Category.class);
        query.select(categoryRoot);
        return em.createQuery(query).getResultList();
    }

    @Override
    @Transactional
    public void deleteById(int id){
        CriteriaBuilder criteriaBuilder=em.getCriteriaBuilder();
        CriteriaDelete<Category> criteriaDelete=criteriaBuilder.createCriteriaDelete(Category.class);
        Root<Category>categoryRoot=criteriaDelete.from(Category.class);
        criteriaDelete.where(criteriaBuilder.equal(categoryRoot.get("id"),id));
        Query query=em.createQuery(criteriaDelete);
        query.executeUpdate();
    }
    @Override
    @Transactional
    public void update(int id){
        CriteriaBuilder criteriaBuilder=em.getCriteriaBuilder();
        CriteriaUpdate<Category> criteriaUpdate=criteriaBuilder.createCriteriaUpdate(Category.class);
        Root<Category>categoryRoot=criteriaUpdate.from(Category.class);
        criteriaUpdate.set("author","abdd");
        criteriaUpdate.where(criteriaBuilder.greaterThanOrEqualTo(categoryRoot.get("id"),id));
        Query query=em.createQuery(criteriaUpdate);
        query.executeUpdate();

    }
}
