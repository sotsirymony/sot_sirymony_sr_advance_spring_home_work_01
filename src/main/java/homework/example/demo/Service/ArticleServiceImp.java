package homework.example.demo.Service;

import homework.example.demo.Model.Articles;
import homework.example.demo.Model.Category;
import homework.example.demo.Repository.ArticleRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class ArticleServiceImp implements ArticleRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public Articles post(Articles articles) {

         //Articles articles1= new Articles(2,"dd","cc","bb",categ);

        // em.persist(new Articles(2,"dd","cc","bb",new Category(1,"mony")) );
         em.persist(articles);
         return articles;
    }

    @Override
    public List<Articles> findAll() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Articles> query = criteriaBuilder.createQuery(Articles.class);
        Root<Articles> bookRoot = query.from(Articles.class);
        query.select(bookRoot);
        return em.createQuery(query).getResultList();
    }

    @Override
    @Transactional
     public void deleteById(int id){
        CriteriaBuilder criteriaBuilder=em.getCriteriaBuilder();
        CriteriaDelete<Articles>criteriaDelete=criteriaBuilder.createCriteriaDelete(Articles.class);
        Root<Articles>articlesRoot=criteriaDelete.from(Articles.class);
        criteriaDelete.where(criteriaBuilder.equal(articlesRoot.get("id"),id));
        Query query=em.createQuery(criteriaDelete);
        query.executeUpdate();
    }
    @Override
    @Transactional
    public void update(int id){
        CriteriaBuilder criteriaBuilder=em.getCriteriaBuilder();
        CriteriaUpdate<Articles> criteriaUpdate=criteriaBuilder.createCriteriaUpdate(Articles.class);
        Root<Articles>articlesRoot=criteriaUpdate.from(Articles.class);
        criteriaUpdate.set("author","abdd");
        criteriaUpdate.where(criteriaBuilder.greaterThanOrEqualTo(articlesRoot.get("id"),id));
        Query query=em.createQuery(criteriaUpdate);
        query.executeUpdate();

    }
}
