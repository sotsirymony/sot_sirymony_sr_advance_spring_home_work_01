package homework.example.demo.Repository;

import homework.example.demo.Model.Articles;
import homework.example.demo.Model.Category;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {
    List<Category> findAll();
    public void deleteById(int id);
    public void update(int id);
    public Category post(Category category);
}
