package homework.example.demo.Repository;

import homework.example.demo.Model.Articles;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository {
    List<Articles> findAll();
    public void deleteById(int id);
    public void update(int id);
    public Articles post(Articles articles);
}
