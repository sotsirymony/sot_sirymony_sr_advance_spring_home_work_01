package homework.example.demo.Controller;

import homework.example.demo.Model.Articles;
import homework.example.demo.Model.Category;
import homework.example.demo.Service.ArticleServiceImp;
import homework.example.demo.Service.CategoryServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1")
public class CategoryRestController {
    @Autowired
    CategoryServiceImp  categoryServiceImp;

    @PostMapping("/categorys")
    public Category postCategory(@RequestBody Category category){
        Category categorys= categoryServiceImp.post(category);
        return categorys;
    }
    @GetMapping("/categorys")
    public ResponseEntity<List<Category>> getCategory()
    {
        return ResponseEntity.ok(categoryServiceImp.findAll());
    }
    @DeleteMapping("/categorys/{id}")
    public ResponseEntity<String>deleteCategoryById(@PathVariable int id){
        categoryServiceImp.deleteById(id);
        return ResponseEntity.ok("deleted success");
    }
    @PutMapping("/categorys/{id}")
    public ResponseEntity<String>updateCategoryById(@PathVariable int id){
        categoryServiceImp.update(id);
        return ResponseEntity.ok("put successfully ");
    }
}
