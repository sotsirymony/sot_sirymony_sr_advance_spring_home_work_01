package homework.example.demo.Controller;

import homework.example.demo.Model.Articles;
import homework.example.demo.Service.ArticleServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@RestController
@RequestMapping("api/v1")
public class ArticleRestController {
    @Autowired
    ArticleServiceImp articleServiceImp;
    @PersistenceContext
    private EntityManager em;
    @PostMapping("/articles")
    public Articles postArticle(@RequestBody Articles articles){
       Articles article= articleServiceImp.post(articles);
        return article;
    }
    @GetMapping("/articles")
    public ResponseEntity<List<Articles>>getArticle()
    {
        return ResponseEntity.ok( articleServiceImp.findAll());
    }
    @DeleteMapping("/articles/{id}")
    public ResponseEntity<String>deleteArticleById(@PathVariable int id){
        articleServiceImp.deleteById(id);
        return ResponseEntity.ok("deleted success");
    }
    @PutMapping("/articles/{id}")
    public ResponseEntity<String>updateArticleById(@PathVariable int id){
        articleServiceImp.update(id);
        return ResponseEntity.ok("put successfully success");
    }
}
