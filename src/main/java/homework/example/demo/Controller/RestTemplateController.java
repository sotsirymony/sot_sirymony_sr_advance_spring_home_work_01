package homework.example.demo.Controller;

import homework.example.demo.Model.Articles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("api/v1")
public class RestTemplateController {
    //resttemplate for articles
    @Autowired
    RestTemplate restTemplate;
    @GetMapping("/resttemplate/articles")
    public String getAll(){
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String>httpEntity= new HttpEntity<String>(httpHeaders);
        return restTemplate.exchange("http://localhost:8082//api/v1/articles", HttpMethod.GET,httpEntity,String.class).getBody();
    }

    @PostMapping("/resttemplate/articles")
    public ResponseEntity<String>create(Articles article){
        //to do save atciles details whtich will generate the articles id
        RestTemplate restTemplate = new RestTemplate();
        final String url = "http://localhost:8082/api/v1/articles";
        Articles addedUser = restTemplate.postForObject(url, article, Articles.class);
        return ResponseEntity.ok("Post success"+ addedUser);
    }
    @PutMapping("/resttemplate/articles/{id}")
    private static void updateEmployee(@PathVariable int id,@RequestBody Articles articles){
        final String uri ="http://localhost:8082//api/v1/articles/{id}";
        RestTemplate restTemplate = new RestTemplate();
        Map<String,Integer> params = new HashMap<String,Integer>();
        params.put("id",id);
        restTemplate.put(uri,articles,params);

    }
    @DeleteMapping("/resttemplate/articles/{id}")
    private static void deleteEmployee(@PathVariable int id){
        final String uri = "http://localhost:8082/api/v1/articles/{id}";
        RestTemplate restTemplate = new RestTemplate();
        Map<String, Integer> params = new HashMap<String, Integer>();
        params.put("id", id);
        restTemplate.delete ( uri,  params );
    }
}
