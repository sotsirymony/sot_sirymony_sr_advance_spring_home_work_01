package homework.example.demo.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name="articles")
public class Articles implements Serializable {
    @Id
    private int id;
    @Column
    private String author;
    @Column
    private String description;
    @Column
    private String title;
    @ManyToOne
    @JoinColumn(name="category_id")
    private Category categorys;

    public Articles() {
    }

    public Articles(int id, String ndkdf, String kjskjdf, String kljsdf, int i) {
    }

    public Articles(int id, String author, String description, String title, Category categorys) {
        this.id = id;
        this.author = author;
        this.description = description;
        this.title = title;
        this.categorys = categorys;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategorys() {
        return categorys;
    }

    public void setCategorys(Category categorys) {
        this.categorys = categorys;
    }

    @Override
    public String toString() {
        return "Articles{" +
                "id=" + id +
                ", author='" + author + '\'' +
                ", description='" + description + '\'' +
                ", title='" + title + '\'' +
                ", categorys=" + categorys +
                '}';
    }

}
